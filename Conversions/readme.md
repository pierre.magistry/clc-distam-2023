# Création du corpus

Le corpus est converti en différents format par les étapes suivantes:


## Du pdf au texte brut

à l'aide du script `./to_text.sh`, qui converti le texte en sinogrammes traditionnels en utilisant `opencc`

## Du texte brut au fichier xml

à l'aide du script `./parse_book_one.py`.

## Chargement du XML en base de données

### création de la base

avec la commande:
`$ basexclient -c "CREATE DB clc_distam"`


### Ajout des fichiers XML à la base

`$ basexclient -c 'OPEN test_distam ; ADD <fichier.xml>'`


## Génération du json pour la plateforme KairnTech

`$ basex ./xml_to_json.xq`

