#!/usr/bin/bash

DBNAME=$1 # distam_train,eval:distam_eval
CONFIG="$2"
K="$3"

for K in $(seq $K)
do
  NAME=$(basename $CONFIG .cfg | sed -s 's/config_//')
  prodigy train --ner "$DBNAME" --lang zh --gpu-id 0 --label-stats -c "$CONFIG" "../Models/$NAME-$K" > "../Models/$NAME-$K.log"
done


