#!/bin/bash

python ../Conversions/sherpa_to_prodigy.py ../Data/sherpa.json  > ../Data/corpus_filtered.jsonl
cat ../Data/corpus_filtered.jsonl | shuf > ../Data/corpus_shuffled.jsonl
head ../Data/corpus_shuffled.jsonl -n -80 > ../Data/corpus_train.jsonl
tail ../Data/corpus_shuffled.jsonl -n 80 | head -n 40 > ../Data/corpus_eval.jsonl
tail ../Data/corpus_shuffled.jsonl -n 40 > ../Data/corpus_test.jsonl

prodigy drop distam_train
prodigy drop distam_eval
prodigy drop distam_test

prodigy db-in distam_train ../Data/corpus_train.jsonl
prodigy db-in distam_eval ../Data/corpus_eval.jsonl
prodigy db-in distam_test ../Data/corpus_test.jsonl
