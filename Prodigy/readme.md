

# Utilisation basique de Prodigy

## lancer prodigy sur les données déjà annotées (test interface)

`$ prodigy ner.manual --label labels-small.txt <nom-du-projet> blank:zh <fichier-données.jsonl>`


## Ajouter les données converties à un projet Prodigy

`$ prodigy db-in <nom-du-projet> <fichier-données.jsonl>`


## courbes d'apprentissage sur les données

`$ prodigy train-curve --ner <nom-du-projet> --lang zh`

(ou `spancat` à la place de `ner`)


# Entraînement de différents models Spacy

Différents fichiers `base_config_*.cfg` on été préparés. 
Le script bash `./init_all_configs.sh` permet de les convertir en fichiers `config_*.cfg` utilisables par Spacy et Prodigy

Example de Lancement d'un entraînement:

`$ prodigy train --ner <données> --lang zh --gpu-id 0 --labels-stats -c <fichier de config.cfg> <dossier où sauvegarder le modèle`

Le script `./train_all.sh` lance l'entraînement de tous les modèles et les sauvegarde dans le dossier ../Models
