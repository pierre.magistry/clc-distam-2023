#!/usr/bin/bash

for D in 100 300 1000
do
  for E in 5 10 30 50 100 500 1000
  do
    if ! [[ $E == 10 && $D == 100 ]]
    then
      CONFIG="../W2V/config_vec_vectors-e$E-d$D.cfg"
      bash train_k_times.sh distam_train,eval:distam_eval "$CONFIG" 20
    fi
  done
done

  



